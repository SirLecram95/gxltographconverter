﻿using GxlToGraphConverter;
using GxlToGraphConverter.DIService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;

namespace GzlToGraphConverter.Starter
{
	class Program
	{
		[STAThread]
		static void Main(string[] args)
		{
			var dependencyContainer = new DependencyContainer();
			var window = DependencyContainer.Container.Resolve<MainWindow>();
			var app = new System.Windows.Application();
			app.Run(window);
		}
	}
}
