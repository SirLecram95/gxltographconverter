﻿using GxlToGraphConverter.Contract;
using GxlToGraphConverter.GraphRepresentation;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace GxlToGraphConverter
{
	public class MatrixGridGenerator : IGenerateGrid
	{
		public Grid GenerateGrid(Graph graphToConvert, Grid grid)
		{
			grid.ColumnDefinitions.Clear();
			grid.RowDefinitions.Clear();
			grid.Children.Clear();
			int nodesCount = graphToConvert.GraphNodes.Count;
			grid.RowDefinitions.Add(new RowDefinition());
			grid.ColumnDefinitions.Add(new ColumnDefinition());
			CreateTable(grid, graphToConvert.GraphNodes);
			var matrix = graphToConvert.Matrix;
			for (int i = 1; i <= nodesCount; i++)
			{
				for(int j = 1; j <= nodesCount; j++)
				{
					var label = GetNewLabel(matrix[i-1, j-1].ToString());
					Grid.SetRow(label, i);
					Grid.SetColumn(label, j);
					grid.Children.Add(label);
				}
			}
			return grid;
		}

		private Label GetNewLabel(string labelContent)
		{
			var label = new Label()
			{
				Content = labelContent,
				HorizontalAlignment = HorizontalAlignment.Center,
				VerticalAlignment = VerticalAlignment.Center,
				FontSize = 20,
			};
			return label;
		}

		private void CreateTable(Grid grid, IEnumerable<GraphNode> nodesList)
		{
			int rowAndColumnNumber = 1;
			foreach (GraphNode graphNode in nodesList)
			{
				grid.RowDefinitions.Add(new RowDefinition());
				grid.ColumnDefinitions.Add(new ColumnDefinition());
				var labelHorizontal = GetNewLabel(graphNode.Id);
				var labelVertical = GetNewLabel(graphNode.Id);
				grid.Children.Add(labelHorizontal);
				grid.Children.Add(labelVertical);
				Grid.SetColumn(labelHorizontal, rowAndColumnNumber);
				Grid.SetRow(labelHorizontal, 0);
				Grid.SetColumn(labelVertical, 0);
				Grid.SetRow(labelVertical, rowAndColumnNumber);
				rowAndColumnNumber++;
			}
		}
	}
}
