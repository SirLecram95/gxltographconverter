﻿using GxlToGraphConverter.CoordinateSystem;
using GxlToGraphConverter.GraphRepresentation;

namespace GxlToGraphConverter.Contract
{
	public interface IAttributeParser
	{
		int ParseRadiusAttribute(GraphAttribute attribute);
		Point ParseCoordinatesAttribute(GraphAttribute attribute);
	}
}