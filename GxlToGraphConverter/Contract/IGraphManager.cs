﻿using GxlToGraphConverter.GraphRepresentation;

namespace GxlToGraphConverter.Contract
{
	public interface IGraphManager
	{
		void LoadGraph();
		void CreateRandomGraph();
		void SaveGraph(Graph graph);

		Graph Graph { get; }
	}
}
