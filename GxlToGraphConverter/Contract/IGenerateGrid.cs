﻿using GxlToGraphConverter.GraphRepresentation;
using System.Windows.Controls;

namespace GxlToGraphConverter.Contract
{
	public interface IGenerateGrid
	{
		Grid GenerateGrid(Graph graphToConvert, Grid grid);
	}
}
