﻿using GxlToGraphConverter.GraphRepresentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GxlToGraphConverter.Contract
{
	public interface IWriteXml
	{
		string WriteGraphToGxl(Graph graphToWrite);
	}
}
