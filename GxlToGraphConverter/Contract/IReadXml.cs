﻿using GxlToGraphConverter.GraphRepresentation;

namespace GxlToGraphConverter.Contract
{
	public interface IReadXml
	{
		Graph Graph { get; }
		Graph ReadGraph(string xmlString);
	}
}
