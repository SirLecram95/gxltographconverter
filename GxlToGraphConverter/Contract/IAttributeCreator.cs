﻿using GxlToGraphConverter.CoordinateSystem;
using GxlToGraphConverter.GraphRepresentation;
using System.Collections.Generic;

namespace GxlToGraphConverter.Contract
{
	public interface IAttributeCreator
	{
		List<GraphAttribute> CreateAttributes(Boundary newNodeBoundary);
	}
}
