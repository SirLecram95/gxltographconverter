﻿using GxlToGraphConverter.Contract;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GxlToGraphConverter
{
	public class FileManager : IFileManager
	{
		private OpenFileDialog openFileDialog;
		private SaveFileDialog saveFileDialog;
		private string filePath;
		public string Content { get; protected set; }

		public FileManager()
		{
			openFileDialog = new OpenFileDialog
			{
				Filter = "Plik GXL (*.gxl)|*.gxl|Plik tekstowy (*.txt)|*.txt|All files (*.*)|*.*"
			};
			saveFileDialog = new SaveFileDialog
			{
				Filter = "Plik GXL (*.gxl)|*.gxl|Plik tekstowy (*.txt)|*.txt|All files (*.*)|*.*",
				AddExtension = true,
				FileName = "graf",
				DefaultExt = ".txt",
			};
			Content = string.Empty;
		}

		public void OpenFile()
		{
			if(openFileDialog.ShowDialog().Value)
			{
				filePath = openFileDialog.FileName;

				using (var fileStream = openFileDialog.OpenFile())
				using (var reader = new StreamReader(fileStream))
				{
					Content = string.Empty;
					Content = reader.ReadToEnd();
				}
			}
		}

		public async void SaveFile(string gxlToSave)
		{
			var result = saveFileDialog.ShowDialog();
			if (result != true)
				return;

			using (var stream = saveFileDialog.OpenFile())
			using (var fileStream = new StreamWriter(stream))
			{
				await fileStream.WriteAsync(gxlToSave);
				//File.WriteAllText(saveFileDialog.FileName, gxlToSave);
			}
		}
	}
}
