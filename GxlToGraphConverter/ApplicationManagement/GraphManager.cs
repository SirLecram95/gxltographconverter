﻿using Autofac;
using GxlToGraphConverter.Additional;
using GxlToGraphConverter.Contract;
using GxlToGraphConverter.DIService;
using GxlToGraphConverter.GraphRepresentation;

namespace GxlToGraphConverter.ApplicationManagement
{
	public class GraphManager : IGraphManager
	{
		private readonly IReadXml GXLReader;
		private readonly IWriteXml GXLWriter;
		public IFileManager fileManager;

		public Graph Graph
		{
			get
			{
				if (graph == null)
					LoadGraph();

				return graph;
			}
		}

		public GraphManager(IReadXml gxlReader, IWriteXml gxlWriter, IFileManager fileManager)
		{
			GXLReader = gxlReader;
			GXLWriter = gxlWriter;
			this.fileManager = fileManager;
		}

		public void LoadGraph()
		{
			fileManager.OpenFile();
			var gxl = fileManager.Content;
			if (gxl != string.Empty)
			{
				GXLReader.ReadGraph(gxl);
				graph = GXLReader.Graph;
				var matrixString = graph.GetMatrixStringRepresentation();
			}
		}

		public void CreateRandomGraph()
		{
			using (var scope = DependencyContainer.Container.BeginLifetimeScope())
			{
				var graphRandomGenerator = scope.Resolve<GraphWithCoordinatesRandomGenerator>();
				graph = graphRandomGenerator.GenerateRandomGraph(10, 2, 1);
			}
		}

		public void SaveGraph(Graph graph)
		{
			var gxlToWrite = GXLWriter.WriteGraphToGxl(graph);
			fileManager.SaveFile(gxlToWrite);
		}

		private Graph graph;
	}
}
