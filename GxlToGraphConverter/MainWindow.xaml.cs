﻿using Autofac;
using GxlToGraphConverter.ApplicationManagement;
using GxlToGraphConverter.Contract;
using GxlToGraphConverter.Gxl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace GxlToGraphConverter
{
	/// <summary>
	/// Logika interakcji dla klasy MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, IView
	{
		private IGraphManager graphManager;
		private IGenerateGrid matrixGridGenerator;

		public MainWindow(IGraphManager graphManager, IGenerateGrid matrixGridGenerator)
		{
			InitializeComponent();
			this.graphManager = graphManager;
			this.matrixGridGenerator = matrixGridGenerator;
		}

		private void ShowGraph()
		{
			matrixGrid = matrixGridGenerator.GenerateGrid(graphManager.Graph, matrixGrid);
			gxlTextBox.Text = graphManager.Graph.GraphGxlRepresentation;
		}

		private void Button_LoadGraphClick(object sender, RoutedEventArgs e)
		{
			graphManager.LoadGraph();
			ShowGraph();
		}

		private void Button_SaveGraphClick(object sender, RoutedEventArgs e)
		{
			if(graphManager.Graph != null)
			{
				graphManager.SaveGraph(graphManager.Graph);
			}
		}

		private void GenerateRandomGraph_Click(object sender, RoutedEventArgs e)
		{
			graphManager.CreateRandomGraph();
			ShowGraph();
		}
	}
}
