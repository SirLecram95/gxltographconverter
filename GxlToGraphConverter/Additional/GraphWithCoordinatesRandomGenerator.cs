﻿using GxlToGraphConverter.Contract;
using GxlToGraphConverter.CoordinateSystem;
using GxlToGraphConverter.GraphRepresentation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GxlToGraphConverter.Additional
{
	public class GraphWithCoordinatesRandomGenerator
	{
		private static Random random;
		private static IAttributeCreator attributeCreator;

		public GraphWithCoordinatesRandomGenerator(IAttributeCreator attributeCreator)
		{
			GraphWithCoordinatesRandomGenerator.attributeCreator = attributeCreator;
		}

		/// <summary>
		/// Metoda tworzy losowy graf spełniający warunki podane w argumentach.
		/// </summary>
		/// <param name="numberOfNodes">Ilość węzłów</param>
		/// <param name="radiusOfAvailableFieldToCreateNodes">Promień okręgu w układzie współrzędnych, wewnątrz którego mają znajdować się środki nowych wierzchołkó</param>
		/// <param name="radiusOfNodeRange">Zasięg pola wierzchołków</param>
		public Graph GenerateRandomGraph(int numberOfNodes, int radiusOfAvailableFieldToCreateNodes, int radiusOfNodeRange)
		{
			// TODO: Dodać generowanie atrybutow zgodnych z generowanym grafem.
			if(random == null) 
				random = new Random(DateTime.Now.Millisecond);

			List<GraphNodeWithBoundary> nodesList = new List<GraphNodeWithBoundary>();
			List<GraphEdge> edgesList = new List<GraphEdge>();

			for(int i = 0; i<numberOfNodes; i++)
			{
				var newNode = CreateNodeWithRandomCoordinatesWithinRange(
					i.ToString(), -radiusOfAvailableFieldToCreateNodes, radiusOfAvailableFieldToCreateNodes, radiusOfNodeRange);

				edgesList.AddRange(CreateEdgesIfNodesRangeIsCrossing(newNode, nodesList));
				nodesList.Add(newNode);
			}

			return new Graph("RandomGraph", nodesList, edgesList);
		}

		/// <summary>
		/// Tworzy wierzchołek w losowym miejscu wewnątrz wyznaczonego pola.
		/// </summary>
		private static GraphNodeWithBoundary CreateNodeWithRandomCoordinatesWithinRange(
			string name, int minCoordinatesValue, int maxCoordinatesValue, int radius)
		{
			var randomXAxis = random.Next(minCoordinatesValue, maxCoordinatesValue + 1);
			var randomYAxis = random.Next(minCoordinatesValue, maxCoordinatesValue + 1);
			var point = new Point(randomXAxis, randomYAxis);
			var field = new Boundary(point, radius);

			var attributesList = attributeCreator.CreateAttributes(field);

			return new GraphNodeWithBoundary(name, field, attributesList);
		}

		private static GraphEdge CreateEdgeBetweenNodes(string edgeName, GraphNode firstNode, GraphNode secondNode)
		{
			GraphEdge edge = new GraphEdge(edgeName, firstNode, secondNode);
			return edge;
		}

		private static List<GraphEdge> CreateEdgesIfNodesRangeIsCrossing(GraphNodeWithBoundary newNode, IEnumerable<GraphNodeWithBoundary> existingNodesList)
		{
			var newNodeBoundary = newNode.Range;
			var nodesWhichColideWithNewNode = existingNodesList.Where(node => CheckIfNodeRangeColideWithNewNodes(newNode.Range, node.Range));
			// TUTAJ TWORZYMY KRAWEDZIE MIEDZY NOWYM NODEM + ISTANIEJACE
			var edgesList = new List<GraphEdge>();
			foreach(GraphNode node in nodesWhichColideWithNewNode)
			{ 
				edgesList.Add(CreateEdgeBetweenNodes(node.Id + newNode.Id, node, newNode));
			}

			return edgesList;
		}

		/// <summary>
		/// Metoda oblicza, czy pole zasięgu jednego wierzchołka koliduje z polem drugiego wierzchołka.
		/// </summary>
		/// <param name="newNodeBoundary">Nowo tworzony node</param>
		/// <param name="existingNodeBoundary">Istniejący node</param>
		private static bool CheckIfNodeRangeColideWithNewNodes(Boundary newNodeBoundary, Boundary existingNodeBoundary)
		{
			var lengthBetweenPoints =
				Math.Sqrt(
					Math.Pow(existingNodeBoundary.CenterBoundary.AxisX - newNodeBoundary.CenterBoundary.AxisX, 2) +
					Math.Pow(existingNodeBoundary.CenterBoundary.AxisY - newNodeBoundary.CenterBoundary.AxisY, 2));

			if (lengthBetweenPoints <= newNodeBoundary.Radius + existingNodeBoundary.Radius)
				return true;
			return false;
		}
	}
}
