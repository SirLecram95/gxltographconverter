﻿using GxlToGraphConverter.Contract;
using GxlToGraphConverter.CoordinateSystem;
using GxlToGraphConverter.GraphRepresentation;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GxlToGraphConverter.Additional
{
	public class GraphAttributesParser : IAttributeParser
	{
		/*public IDictionary<NodeAttribute, object> ParseAttributes(IEnumerable<GraphAttribute> attributes)
		{
			Dictionary<NodeAttribute, object> attributesDictionary = new Dictionary<NodeAttribute, object>();

			foreach(var attribute in attributes)
			{
				var attributeName = attribute.Name;
				switch
			}
			return attributesDictionary;
		}*/
		public int ParseRadiusAttribute(GraphAttribute attribute)
		{
			if (attribute.AttributeType != NodeAttribute.Radius)
				throw new System.Exception("Błąd, zły typ atrybutu - spodziewany 'Radius'.");

			var radiusAttribute = attribute;
			var r = int.Parse(radiusAttribute.Value);
			return r;
		}

		public Point ParseCoordinatesAttribute(GraphAttribute attribute)
		{
			if (attribute.AttributeType != NodeAttribute.Location)
				throw new System.Exception("Błąd, zły typ atrybutu - spodziewany 'Location'.");

			var pointAttribute = attribute;
			var coordinates = pointAttribute.Value.Split(';');
			var x = int.Parse(coordinates[0]);
			var y = int.Parse(coordinates[1]);

			return new Point(x, y);
		}
	}
}
