﻿using System.Collections.Generic;
using GxlToGraphConverter.Contract;
using GxlToGraphConverter.CoordinateSystem;
using GxlToGraphConverter.GraphRepresentation;

namespace GxlToGraphConverter.Additional
{
	public class GraphAttributesCreator : IAttributeCreator
	{
		public List<GraphAttribute> CreateAttributes(Boundary newNodeBoundary)
		{
			var attributesList = new List<GraphAttribute>();
			var x = newNodeBoundary.CenterBoundary.AxisX;
			var y = newNodeBoundary.CenterBoundary.AxisY;
			var r = newNodeBoundary.Radius;
			attributesList.Add(new GraphAttribute("location", x + ";" + y, NodeAttribute.Location));
			attributesList.Add(new GraphAttribute("radius", r.ToString(), NodeAttribute.Radius));

			return attributesList;
		}
	}
}
