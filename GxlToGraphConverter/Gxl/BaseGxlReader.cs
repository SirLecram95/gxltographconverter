﻿using GxlToGraphConverter.Additional;
using GxlToGraphConverter.Contract;
using GxlToGraphConverter.GraphRepresentation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace GxlToGraphConverter.Gxl
{
	public class BaseGxlReader : IReadXml
	{
		public Graph Graph { get; protected set; }
		public XmlDocument XmlDoc { get; protected set; }

		public BaseGxlReader()
		{
			graphEdges = new List<GraphEdge>();
			graphNodes = new List<GraphNode>();
		}
		public BaseGxlReader(string gxlGraph)
		{
			LoadXml(gxlGraph);
		}

		public Graph ReadGraph(string xmlString)
		{
			XmlDoc = LoadXml(xmlString);
			if (!XmlDoc.HasChildNodes)
				return null;

			graphNodes.Clear();
			graphEdges.Clear();

			ReadNodesList();
			ReadEdgesList();
			var graphName = ReadGraphName();

			Graph = new Graph(graphName, graphNodes, graphEdges, XmlDoc);
			return Graph;
		}

		private XmlDocument LoadXml(string gxlGraph)
		{
			var xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(gxlGraph);
			return xmlDoc;
		}
		// Dodac jako parametr typ, który bedzie decydowal o tym jaki Graf stworzyc
		protected void ReadNodesList()
		{
			var nodes = XmlDoc.GetElementsByTagName(NodePropertyName);

			foreach (XmlNode node in nodes)
			{
				var nodeName = node.Attributes.GetNamedItem(IdAttributeName).InnerText;
				var nodeXml = LoadXml(node.OuterXml);
				var newNode = ReadNode(nodeName, nodeXml);
				graphNodes.Add(newNode);
			}
		}

		protected virtual GraphNode ReadNode(string nodeName, XmlDocument document)
		{
			var lookingForAttributesDictionary = new Dictionary<NodeAttribute, string>();
			lookingForAttributesDictionary.Add(NodeAttribute.Location, "location");
			lookingForAttributesDictionary.Add(NodeAttribute.Radius, "radius");

			var attributes = ReadNodeAttributes(document, lookingForAttributesDictionary);

			return new GraphNode(nodeName, attributes);
		}

		private void ReadEdgesList()
		{
			var edges = XmlDoc.GetElementsByTagName(EdgePropertyName);
			graphEdges = new List<GraphEdge>();

			foreach (XmlNode edge in edges)
			{
				var id = edge.Attributes.GetNamedItem(IdAttributeName).InnerText;
				var toNodeId = edge.Attributes.GetNamedItem("to").InnerText;
				var toNode = graphNodes.FirstOrDefault(node => node.Id == toNodeId);
				var fromNodeId = edge.Attributes.GetNamedItem("from").InnerText;
				var fromNode = graphNodes.FirstOrDefault(node => node.Id == fromNodeId);

				graphEdges.Add(new GraphEdge(id, fromNode, toNode));
			}
		}

		private List<GraphAttribute> ReadNodeAttributes(XmlDocument nodeXmlDocument, IDictionary<NodeAttribute, string> attributesToReadName)
		{
			var attributes = new List<GraphAttribute>();
			var attributesNode = nodeXmlDocument.GetElementsByTagName("attr").Cast<XmlNode>();

			XmlNode xmlNode;
			foreach(string attributeName in attributesToReadName.Values)
			{
				var index = attributesToReadName.Values.ToList().IndexOf(attributeName);

				xmlNode = attributesNode.Where(n => n.Attributes.GetNamedItem("name").InnerText == attributeName).FirstOrDefault();
				if(xmlNode != null)
					attributes.Add(new GraphAttribute(attributeName, xmlNode.InnerText, attributesToReadName.Keys.ElementAt(index)));
			}
			return attributes;
		}

		private string ReadGraphName()
		{
			string graphName = string.Empty;
			var node = XmlDoc.GetElementsByTagName(GraphPropertyName);
			if (node != null && node.Item(0) != null)
				graphName = node[0].Attributes.GetNamedItem(IdAttributeName).InnerText;
			return graphName;
		}

		private IList<GraphNode> graphNodes;
		private IList<GraphEdge> graphEdges;
		private readonly string NodePropertyName = "node";
		private readonly string EdgePropertyName = "edge";
		private readonly string GraphPropertyName = "graph";
		private readonly string IdAttributeName = "id";
	}
}
