﻿using System.Linq;
using System.Xml;
using GxlToGraphConverter.GraphRepresentation;

namespace GxlToGraphConverter.Gxl
{
	class GraphWithBoundaryReader : BaseGxlReader
	{
		protected override GraphNode ReadNode(string nodeName, XmlDocument document)
		{
			var node = base.ReadNode(nodeName, document);

			if (node.GraphAttributes.Any())
				return new GraphNodeWithBoundary(node);
			else
				return node;
		}
	}
}
