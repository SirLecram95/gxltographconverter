﻿using GxlToGraphConverter.GraphRepresentation;
using System.Xml;
using System.Linq;
using System.Collections.Generic;
using GxlToGraphConverter.Contract;
using System;

namespace GxlToGraphConverter.Gxl
{
	public class BaseGxlWriter : IWriteXml
	{
		private XmlDocument xmlDoc;
		public Graph Graph { get; protected set; }

		public BaseGxlWriter()
		{
			// Dodać abstrakcję - GraphConverter czy cos
		}

		public string WriteGraphToGxl(Graph graphToWrite)
		{
			xmlDoc = new XmlDocument();
			var rootElement = xmlDoc.CreateElement("gxl");
			xmlDoc.AppendChild(rootElement);

			var graphElement = CreateGraphElement(graphToWrite, rootElement);

			WriteNodesList(graphToWrite.GraphNodes, graphElement);
			WriteEdgesList(graphToWrite.GraphEdges, graphElement);

			return xmlDoc.OuterXml.Replace("><", ">" + Environment.NewLine +  "<");
		}

		private void WriteNodesList(IEnumerable<GraphNode> graphNodes, XmlNode rootElement)
		{
			foreach (GraphNode n in graphNodes)
			{
				var newNode = xmlDoc.CreateElement("node");
				var attr = xmlDoc.CreateAttribute("id");
				attr.Value = n.Id;
				newNode.Attributes.Append(attr);

				WriteNodeAttributes(n.GraphAttributes, rootElement.AppendChild(newNode));
			}
		}

		private void WriteEdgesList(IEnumerable<GraphEdge> graphEdges, XmlNode rootElement)
		{
			foreach (GraphEdge edge in graphEdges)
			{
				var newNode = xmlDoc.CreateElement("edge");
				var attr = xmlDoc.CreateAttribute("id");
				attr.Value = edge.Id;
				var attrArray = new List<XmlAttribute> { xmlDoc.CreateAttribute("from"), xmlDoc.CreateAttribute("to"), };
				int i = 0;
				attrArray.ForEach(a =>
				{
					a.Value = edge.Nodes[i].Id;
					i++;
				});
				newNode.Attributes.Append(attr);
				newNode.Attributes.Append(attrArray[0]);
				newNode.Attributes.Append(attrArray[1]);
				rootElement.AppendChild(newNode);
			}
		}

		private XmlNode CreateGraphElement(Graph graphToWrite, XmlNode rootElement)
		{
			var attribute = xmlDoc.CreateAttribute("id");
			var node = xmlDoc.CreateElement("graph");
			node.Attributes.Append(attribute);
			attribute.Value = graphToWrite.Id;
			var graphElement = rootElement.AppendChild(node);
			return graphElement;
		}

		private void WriteNodeAttributes(IEnumerable<GraphAttribute> nodeAttributes, XmlNode rootElement)
		{
			if (!nodeAttributes.Any())
				return;

			foreach(GraphAttribute attribute in nodeAttributes)
			{
				var attributeNode = xmlDoc.CreateElement("attr");
				var nodeName = xmlDoc.CreateAttribute("name");
				attributeNode.Attributes.Append(nodeName);
				nodeName.Value = attribute.Name;
				var newRootElement = rootElement.AppendChild(attributeNode);

				var innerNode = xmlDoc.CreateElement("string");
				innerNode.InnerText = attribute.Value;
				newRootElement.AppendChild(innerNode);
			}
		}
	}
}
