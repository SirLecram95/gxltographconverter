﻿using Autofac;
using GxlToGraphConverter.Additional;
using GxlToGraphConverter.ApplicationManagement;
using GxlToGraphConverter.Contract;
using GxlToGraphConverter.Gxl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GxlToGraphConverter.DIService
{
	public class DependencyContainer
	{
		public static IContainer Container;

		public DependencyContainer()
		{
			RegisterContainer();
		}

		private void RegisterContainer()
		{
			var builder = new ContainerBuilder();
			builder.RegisterType<GraphAttributesParser>().As<IAttributeParser>();
			builder.RegisterType<GraphAttributesCreator>().As<IAttributeCreator>();
			builder.RegisterType<GraphWithCoordinatesRandomGenerator>().AsSelf(); //Do zmiany + interface
			builder.RegisterType<GraphManager>().As<IGraphManager>();
			builder.RegisterType<BaseGxlReader>().As<IReadXml>().AsSelf();
			builder.RegisterType<GraphWithBoundaryReader>().As<IReadXml>();
			builder.RegisterType<BaseGxlWriter>().AsSelf().As<IWriteXml>();
			builder.RegisterType<FileManager>().As<IFileManager>();
			builder.RegisterType<MatrixGridGenerator>().As<IGenerateGrid>();
			builder.RegisterType<MainWindow>().As<IView>().AsSelf();
			builder.RegisterAssemblyModules();
			Container = builder.Build();
		}
	}
}
