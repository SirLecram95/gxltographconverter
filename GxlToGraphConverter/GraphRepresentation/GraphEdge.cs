﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GxlToGraphConverter.GraphRepresentation
{
	public class GraphEdge
	{
		public string Id { get; protected set; }
		public GraphNode[] Nodes { get; protected set; }

		public GraphEdge(string id, GraphNode firstNode, GraphNode secondNode)
		{
			Id = id;
			Nodes = new[] { firstNode, secondNode };
		}

	}
}
