﻿using Autofac;
using GxlToGraphConverter.Additional;
using GxlToGraphConverter.Contract;
using GxlToGraphConverter.CoordinateSystem;
using GxlToGraphConverter.DIService;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GxlToGraphConverter.GraphRepresentation
{
	class GraphNodeWithBoundary : GraphNode
	{
		public Boundary Range { get; protected set; }

		public GraphNodeWithBoundary(string id, Boundary range, IEnumerable<GraphAttribute> attributes)
			:base(id, attributes)
		{
			Range = range;
		}

		public GraphNodeWithBoundary(string id, Boundary range)
			: base(id, new List<GraphAttribute>())
		{
			Range = range;
		}

		public GraphNodeWithBoundary(GraphNode baseNode)
			:base(baseNode.Id, baseNode.GraphAttributes)
		{
			Point pointAttribute; int radiusAttribute;

			var pointAttr = GraphAttributes.Where(attr => attr.AttributeType == NodeAttribute.Location).FirstOrDefault();
			var radiusAttr = GraphAttributes.Where(attr => attr.AttributeType == NodeAttribute.Radius).FirstOrDefault();
			if (pointAttr == null && radiusAttr == null)
				throw new Exception("Puste wartosci w GraphNodeWithBoundary");

			using (var scope = DependencyContainer.Container.BeginLifetimeScope())
			{
				var parser = DependencyContainer.Container.Resolve<IAttributeParser>();
				pointAttribute = parser.ParseCoordinatesAttribute(pointAttr);
				radiusAttribute = parser.ParseRadiusAttribute(radiusAttr);
			}

			Range = new Boundary(pointAttribute, radiusAttribute);
		}
	}
}