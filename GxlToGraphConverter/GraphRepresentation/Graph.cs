﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GxlToGraphConverter.GraphRepresentation
{
	public class Graph
	{
		public string Id { get; protected set; }
		public List<GraphNode> GraphNodes { get; protected set; }
		public List<GraphEdge> GraphEdges { get; protected set; }
		private XmlDocument gxlGraphRepresentation;
		public string GraphGxlRepresentation { get => gxlGraphRepresentation != null ? gxlGraphRepresentation.OuterXml.Replace("><", ">\n<") : String.Empty; }
		public int[,] Matrix { get; protected set; }

		public Graph(string name, IEnumerable<GraphNode> graphNodes, IEnumerable<GraphEdge> graphEdges, XmlDocument gxlDocument)
			:this(name, graphNodes, graphEdges)
		{
			gxlGraphRepresentation = gxlDocument;
		}
		public Graph(string name, IEnumerable<GraphNode> graphNodes, IEnumerable<GraphEdge> graphEdges)
		{
			Id = name;
			this.GraphNodes = graphNodes.ToList();
			this.GraphEdges = graphEdges.ToList();
			Matrix = CreateMatrix();
		}
		public string GetMatrixStringRepresentation()
		{
			return CreateStringRepresentationOfMatrix(Matrix, GraphNodes.Select(node => node.Id));
		}
		private string CreateStringRepresentationOfMatrix(int[,] matrix, IEnumerable<string> nodeNames)
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("X | ");
			foreach(string node in nodeNames)
			{
				builder.Append(node + " | ");
			}
			builder.Append(Environment.NewLine);
			var nodesCount = GraphNodes.Count;
			for(int i = 0; i < nodesCount; i++)
			{
				for(int j = 0; j<nodesCount; j++)
				{
					if(j == 0)
					{
						builder.Append(nodeNames.ElementAt(i) + " | ");
					}
					builder.Append(matrix[i, j] + " | ");
				}
				builder.Append(Environment.NewLine);
			}
			return builder.ToString();
		}

		private int[,] CreateMatrix()
		{
			var matrixRowsAndColumnsCount = GraphNodes.Count;
			var matrix = new int[matrixRowsAndColumnsCount,matrixRowsAndColumnsCount];
			for(int i = 0; i<matrixRowsAndColumnsCount; i++)
			{
				for(int j = 0; j<matrixRowsAndColumnsCount; j++)
				{
					if (i != j)
					{
						var edges = GraphEdges.Where(graph => graph.Nodes.Contains(GraphNodes[i]) && graph.Nodes.Contains(GraphNodes[j]));
						matrix[i, j] = edges.Count();
					}
					else
					{
						matrix[i, j] = 0;
					}
				}
			}
			return matrix;
		}
	}
}
