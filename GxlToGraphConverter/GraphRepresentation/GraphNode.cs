﻿using System.Collections.Generic;
using System.Linq;

namespace GxlToGraphConverter.GraphRepresentation
{
	public class GraphNode
	{
		public string Id { get; protected set; }
		public List<GraphAttribute> GraphAttributes { get; protected set; } 

		public GraphNode(string id, IEnumerable<GraphAttribute> attributes)
		{
			this.Id = id;
			this.GraphAttributes = attributes.ToList();
		}
	}
}
