﻿using GxlToGraphConverter.Additional;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GxlToGraphConverter.GraphRepresentation
{
	public class GraphAttribute
	{
		public string Value { get; protected set; }
		public string Name { get; protected set; }
		public NodeAttribute AttributeType { get; protected set; }

		public GraphAttribute(string name, string value, NodeAttribute attributeType)
		{
			Value = value;
			Name = name;
			AttributeType = attributeType;
		}
	}
}
