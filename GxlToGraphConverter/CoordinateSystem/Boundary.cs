﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GxlToGraphConverter.CoordinateSystem
{
	public class Boundary
	{
		public int Radius { get; protected set; }
		public Point CenterBoundary { get; protected set; }

		public Boundary(Point center, int radius)
		{
			CenterBoundary = center;
			Radius = radius;
		}
	}
}
