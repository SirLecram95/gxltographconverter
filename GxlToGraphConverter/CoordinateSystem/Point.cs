﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GxlToGraphConverter.CoordinateSystem
{
	public class Point
	{
		public int AxisX { get; protected set; }
		public int AxisY { get; protected set; }

		public Point(int axisX, int axisY)
		{
			AxisX = axisX;
			AxisY = axisY;
		}
	}
}
